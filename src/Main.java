import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
public class Main {
    static Connection conn;
    public static void main(String[] arts) throws SQL{
        conn =
                DriverManager.getConnection("jdbc:mysql://localhost:3306/student_info");
        String SQL_QUERY =String.format (
                """
                SELECT last_name, first_name, sur_name, AVG(mark) AS "AVERAGE MARKS"
                FROM student
                JOIN grade_book ON student.id = grade_book.student_id
                GROUP BY student.id;
                """);
        PreparedStatement studentsQuerry = conn.prepareStatement(SQL_QUERY);

        ResultSet studentsSet = studentsQuerry.executeQuery();

        System.out.println("Фамилия " + "-".repeat(10) + " Имя " + "-".repeat(10) + " Отчество" + "-".repeat(10) + "Оценка" + "-".repeat(10));
        while (studentsSet.next()) {
            System.out.println(studentsSet.getString(1) + " " + studentsSet.getString(2) + " " + studentsSet.getString(3) + " " +  studentsSet.getString(12));
        }
    }
}


